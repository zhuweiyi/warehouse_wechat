// app.js
var client = require('./utils/client.js')
App({
  onLaunch: function () {
    if (wx.canIUse('getUpdateManager')) {
      const updateManager = wx.getUpdateManager()
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        if (res.hasUpdate) {
          updateManager.onUpdateReady(function () {
            wx.showModal({
              title: '更新提示',
              content: '新版本已经准备好，是否重启应用？',
              success: function (res) {
                // res: {errMsg: "showModal: ok", cancel: false, confirm: true}
                if (res.confirm) {
                  // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                  updateManager.applyUpdate()
                }
              }
            })
          })
          updateManager.onUpdateFailed(function () {
            // 新的版本下载失败
            wx.showModal({
              title: '已经有新版本了哟',
              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~'
            })
          })
        }
      })
    }
  },
  alert({ title = '提示', content, confirm }) {
    wx.showModal({
      showCancel: false,
      title: title,
      content: content,
      success(res) {
        if (res.confirm) {
          confirm && confirm()
        }
      }
    })

  },
  confirm({ title = '提示', content, confirm }) {
    wx.showModal({
      title: title,
      content: content,
      success(res) {
        if (res.confirm) {
          confirm && confirm()
        }
      }
    })
  },
  post({ url, data, success, showLoading = true }) {
    client.post({
      url: this.base_url() + url,
      data: data,
      showLoading: showLoading,
      onSuccess: success
    })
  },
  get({ url, success, showLoading = true }) {
    client.get({
      url: this.base_url() + url,
      showLoading: showLoading,
      onSuccess: success
    })
  },
  uploadImage({ success, showLoading = true }) {
    var that = this;
    function fromCamera() {
      wx.chooseImage({
        count: 9,
        sizeType: ['original', 'compressed'],
        sourceType: ['album', 'camera'],
        success(res) {
          const tempFilePaths = res.tempFilePaths;
          for (let index = 0; index < tempFilePaths.length; index++) {
            const element = tempFilePaths[index];
            client.uploadImage({
              url: that.base_url() + "/file/filesUpload",
              showLoading: showLoading,
              filePath: element,
              onSuccess: success,
            })
          }

        }
      })
    }
    function fromHistory() {
      wx.chooseMessageFile({
        count: 9,
        type: 'image',
        success(res) {

          // tempFilePath可以作为img标签的src属性显示图片
          const tempFilePaths = res.tempFiles
          console.log(tempFilePaths)
          for (let index = 0; index < tempFilePaths.length; index++) {
            const element = tempFilePaths[index];
            client.uploadImage({
              url: that.base_url() + "/file/filesUpload",
              showLoading: showLoading,
              filePath: element.path,
              onSuccess: success,
            })
          }
        }
      })
    }


    wx.showActionSheet({
      itemList: ['拍照', '相册', '从聊天记录中选择'],
      success(res) {
        if (res.tapIndex == 0) {
          fromCamera();
        } else if (res.tapIndex == 2) {
          fromHistory();
        }
      }
    })



  },
  log(msg) {
    console.log(msg);
  },
  base_url() {
    // return "http://localhost:20191";
    return "http://gtj.zdwhly.com/APPAPI/";
  },
  img_url() {
    return `${this.base_url()}/files/`;
  },
  url: {
    login: "UserHandlerAPI.ashx?action=MemberLogin", //登录
  },
  globalData: {
    user: {}
  }
})
