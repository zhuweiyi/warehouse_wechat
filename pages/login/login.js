// pages/login/login.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    account: '',
    pwd: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  accountInput(e) {
    this.setData({
      account: e.detail.value
    })
  },
  pwdInput(e) {
    this.setData({
      pwd: e.detail.value
    })
  },
  /**
   * 登录
   */
  login() {
    if (!this.data.account) {
      app.alert({
        content: '请输入账号'
      })
      return;
    }

    if (!this.data.pwd) {
      app.alert({
        content: '请输入密码'
      })
      return;
    }
    app.post({
      url: app.url.login,
      data: {
        Loginname: this.data.idcard,
        password: this.data.pwd
      },
      success: (res) => {
        if (res.status) {
          res.data.pwd = this.data.pwd;
          wx.setStorageSync('userinfo', res.data);
          wx.setStorageSync('loginId', res.data.useridcard);
          app.globalData.user = res.data;
          wx.reLaunch({
            url: '/pages/main/main',
          })
        } else {
          if (this.data.needpwd) {
            app.alert({
              content: res.msg
            })
          } else {
            app.alert({
              content: "登录失败"
            })
          }

        }
      }
    })
  }
})