function get({
  url,
  onSuccess,
  onError,
  showLoading = true
}) {
  if (showLoading) {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
  }
  wx.request({
    header: {
      "token": getApp().globalData.token
    },
    url: url,
    success: (res) => {
      if (res.data.code == 10000) {
        onSuccess && onSuccess(res.data.result);
      } else if (res.data.code == 10002) {
        wx.showModal({
          title: "提示",
          content: "登录失效，点击重新登录",
          showCancel: false,
          success(res) {
            if (res.confirm) {
              wx.reLaunch({
                url: '/pages/login/login',
              })
            }
          }
        })
      } else {
        wx.showModal({
          title: "提示",
          showCancel: false,
          content: res.data.msg ? res.data.msg : '提交数据异常'
        })
      }
    },
    fail: () => {

    },
    complete: () => {
      if (showLoading) {
        wx.hideLoading();
      }
    }
  })
}

function post({
  url,
  data,
  onSuccess,
  onError,
  showLoading = true
}) {
  if (showLoading) {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
  }
  wx.request({
    method: 'POST',
    url: url,
    data: data,
    header: {
      'content-type': 'application/x-www-form-urlencoded', // 默认值
      "token": getApp().globalData.token
    },
    success: (res) => {
      if (res.data.code == 10000) {
        onSuccess && onSuccess(res.data.result);
      } else if (res.data.code == 10002) {
        wx.showModal({
          title: "提示",
          content: "登录失效，点击重新登录",
          showCancel: false,
          success(res) {
            if (res.confirm) {
              wx.reLaunch({
                url: '/pages/login/login',
              })
            }
          }
        })
      } else {
        wx.showModal({
          title: "提示",
          showCancel: false,
          content: res.data.msg ? res.data.msg : '提交数据异常'
        })
      }

    },
    fail: () => {

    },
    complete: () => {
      if (showLoading) {
        wx.hideLoading();
      }
    }
  })
}

function uploadImage({
  url,
  filePath,
  onSuccess,
  onError,
  showLoading = true
}) {
  if (showLoading) {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
  }
  wx.uploadFile({
    url: url,
    header: {
      "token": getApp().globalData.token
    },
    filePath: filePath,
    name: 'files',
    success(res) {
      console.log(res)
      var data = JSON.parse(res.data)
      onSuccess && onSuccess(data.result);
    },
    fail: () => {

    },
    complete: () => {
      if (showLoading) {
        wx.hideLoading();
      }
    }
  })
}


module.exports = {
  get: get,
  post: post,
  uploadImage: uploadImage
}